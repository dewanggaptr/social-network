class Api::V1::CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :update, :destroy]

  def index
    @comment = Comment.all

    render json: @comment.map { |comment| comment.new_attributes }
  end

  def show
    render json: @comment.new_attributes
  end

  def create
    @comment = Comment.new(comment_params)

    if @comment.save
      render json: @comment.new_attributes, status: :created
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end

  def update
    if @comment.update(comment_params)
      render json: @comment.new_attributes
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @comment.destroy
  end

  private
  def set_comment
    @comment = Comment.find_by_id(params[:id])
    if @comment.nil?
      render json: { error: "Comment not found" }, status: 404
    end
  end

  def comment_params
    params.require(:comment).permit(:comment_text, :profile_id, :post_id, :parent_id)
  end
end
