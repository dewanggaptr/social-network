class Api::V1::PostsController < ApplicationController
  before_action :set_post, only: [:show, :update, :destroy]

  def index
    @post = Post.all

    render json: @post.map { |post| post.new_attributes }
  end

  def show
    render json: @post.new_attributes
  end

  def create
    @post = Post.new(post_params)

    if @post.save
      render json: @post.new_attributes, status: :created
    else
      render json: @post.errors, status: :unprocessable_entity
    end
  end

  def update
    if @post.update(post_params)
      render json: @post.new_attributes
    else
      render json: @post.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @post.destroy
  end

  private
  def set_post
    @post = Post.find_by_id(params[:id])
    if @post.nil?
      render json: { error: "Post not found "}, status: :not_found
    end
  end

  def post_params
    params.require(:post).permit(:caption, :tipe, :profile_id, :room_id)
  end
end
