class Api::V1::UsersController < ApplicationController
  skip_before_action :authenticate_request, only: [:create, :login]
  before_action :set_user, only: [:show, :update, :destroy]

  def index
    @user = User.all

    render json: @user.map { |user| user.new_attributes }
  end

  def show
    render json: @user.new_attributes
  end

  def create
    @user = User.new(user_params)

    if @user.save
      render json: @user.new_attributes, status: :created
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def update
    if @user.update(user_params)
      render json: @user.new_attributes
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @user.destroy
  end

  def login
    @user = User.find_by(email: params[:email])
    if @user && @user&.authenticate(params[:password])
      token = JsonWebToken.encode(user_id: @user.id)
      render json: {
        user: @user.new_attributes,
        token: token,
      }
    else
      render json: { error: "Invalid username or password" }, status: :unauthorized
    end
  end

  private
  def set_user
    @user = User.find_by_id(params[:id])

    if @user.nil?
      render json: { error: "User not found" }, status: :not_found
    end
  end

  def user_params
    params.require(:user).permit(:email, :password)
  end
end
