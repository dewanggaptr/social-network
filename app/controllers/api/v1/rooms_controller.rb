class Api::V1::RoomsController < ApplicationController
  before_action :set_room, only: [:show, :update, :destroy]
  
  def index
    @room = Room.all

    render json: @room.map { |room| room.new_attributes }
  end

  def show
    render json: @room.new_attributes
  end

  def create
    @room = Room.new(room_params)

    if @room.save
      render json: @room.new_attributes, status: :created
    else
      render json: @room.new_attributes, status: :unprocessable_entity
    end
  end

  def update
    if @room.update(room_params)
      render json: @room.new_attributes
    else
      render json: @room.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @room.destroy
  end

  private
  def set_room
    @room = Room.find_by_id(params[:id])
    if @room.nil?
      render json: { error: "Room not found" }, status: :not_found
    end
  end

  def room_params
    params.require(:room).permit(:title, :description)
  end
end
