class Api::V1::ProfilesController < ApplicationController
  before_action :set_profile, only: [:show, :update, :destroy]
  
  def index
    @profile = Profile.all

    render json: @profile.map { |profile| profile.new_attributes }
  end

  def show
    render json: @profile.new_attributes
  end

  def create
    @profile = Profile.new(profile_params)

    if @profile.save
      render json: @profile.new_attributes, status: :created
    else
      render json: @profile.errors, status: :unprocessable_entity
    end
  end

  def update
    if @profile.update(profile_params)
      render json: @profile.new_attributes
    else
      render json: @profile.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @profile.destroy
  end

  private
  def set_profile
    @profile = Profile.find_by_id(params[:id])
    if @profile.nil?
      render json: { error: "Profile not found" }, status: :not_found
    end
  end

  def profile_params
    params.require(:profile).permit(:profile_name, :phone_number, :dob, :gender, :bio, :user_id)
  end
end
