class Api::V1::LikesController < ApplicationController
  before_action :set_like, only: [:show, :update, :destroy]

  def index
    @like = Like.all

    render json: @like
  end

  def create
    @like = Like.new(like_params)

    if @like.save
      render json: @like, status: 201
    else
      render json: @like.errors, status: 422
    end
  end

  def destroy
    @like.destroy
  end

  private
  def set_like
    @like = Like.find_by_id(params[:id])
    if @like.nil?
      render json: { error: "Like not foud" }, status: 404
    end
  end

  def like_params
    params.require(:like).permit(:profile_id, :likers_type, :likers_id)
  end
end
