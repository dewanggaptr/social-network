class Api::V1::FollowsController < ApplicationController
  before_action :set_follow, only: [:show, :update, :destroy]

  def index
    @follow = Follow.all

    render json: @follow.map { |follow| follow.new_attributes }
  end

  def create
    @follow = Follow.new(follow_params)

    if @follow.save
      render json: @follow.new_attributes, status: :created
    else
      render json: @follow.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @follow.destroy
  end

  private
  def set_follow
    @follow = Follow.find_by_id(params[:id])
    if @follow.nil?
      render json: { error: "Follow not found" }, status: 404
    end
  end

  def follow_params
    params.require(:follow).permit(:follower_id, :following_id)
  end
end
