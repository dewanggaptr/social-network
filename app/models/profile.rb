class Profile < ApplicationRecord
  belongs_to :user
  has_many :posts, dependent: :destroy
  has_many :followers, class_name: "Follow", foreign_key: "follower_id", dependent: :destroy
  has_many :followings, class_name: "Follow", foreign_key: "following_id", dependent: :destroy

  validates :profile_name, presence: true, length: { maximum: 50 },
                           uniqueness: true
  validates :phone_number, presence: true, length: { maximum: 15 },
                           format: { with: /\A[0-9]+\z/ },
                           uniqueness: true
  validates :dob, presence: true
  validates :gender, presence: true
  validates :bio, length: { maximum: 500 }
  validates :user_id, presence: true

  enum gender: {
    male: 1,
    female: 2
  }
  
  def new_attributes
    {
      id: self.id,
      profile_name: self.profile_name,
      phone_number: self.phone_number,
      dob: self.dob,
      gender: self.gender,
      bio: self.bio,
      user: self.user,
      created_at: self.created_at
    }
  end
end
