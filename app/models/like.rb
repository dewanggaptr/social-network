class Like < ApplicationRecord
  belongs_to :profile
  belongs_to :likers, polymorphic: true

  validates :profile_id, presence: true
  validates :likers_type, presence: true
  validates :likers_id, presence: true

  def new_attributes
    {
      id: self.id,
      profile: self.profile,
      likers_type: self.like_type,
      likers_id: self.likers_id
    }
  end
end
