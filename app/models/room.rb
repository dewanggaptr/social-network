class Room < ApplicationRecord
  has_many :posts

  validates :title, presence: true, length: { maximum: 30 }
  validates :description, presence: true, length: { maximum: 100 }

  def new_attributes
    {
      id: self.id,
      title: self.title,
      description: self.description,
      created_at: self.created_at
    }
  end
end
