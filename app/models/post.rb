class Post < ApplicationRecord
  belongs_to :profile
  belongs_to :room
  has_many :comments, dependent: :destroy
  has_many :likes, as: :likers, dependent: :destroy
  
  validates :caption, presence: true, length: { maximum: 500 }
  validates :tipe, presence: true
  validates :profile_id, presence: true
  validates :room_id, presence: true
    
  enum tipe: {
    visible: 1,
    invisible: 2
  }
  
  def new_attributes
    {
      id: self.id,
      caption: self.caption,
      tipe: self.tipe,
      profile: self.profile&.new_attributes,
      room: self.room,
      likes_count: self.likes.count,
      likers: self.likes.map { |like| like.profile},
      created_at: self.created_at
    }
  end
end
