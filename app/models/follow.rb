class Follow < ApplicationRecord
  belongs_to :follower, class_name: "Profile", foreign_key: "follower_id"
  belongs_to :following, class_name: "Profile", foreign_key: "following_id"
  
  validates :follower_id, presence: true
  validates :following_id, presence: true

  def new_attributes
    {
      id: self.id,
      follower_id: self.follower_id,
      following_id: self.following_id
    }
  end
end
