class User < ApplicationRecord
  has_one :profile, dependent: :destroy
  has_secure_password

  validates :email, presence: true, length: { maximum: 50 },
                    format: { with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i },
                    uniqueness: true

  def new_attributes
    {
      id: self.id,
      email: self.email,
      password_digest: self.password_digest,
      created_at: self.created_at
    }
  end
end
