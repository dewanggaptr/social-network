class Comment < ApplicationRecord
  belongs_to :profile
  belongs_to :post
  belongs_to :parent, class_name: "Comment", foreign_key: "parent_id", optional: true
  has_many :children, class_name: "Comment", foreign_key: "parent_id", dependent: :destroy
  has_many :likes, as: :likers, dependent: :destroy
  
  validates :comment_text, presence: true, length: { maximum: 200 }
  validates :profile_id, presence: true
  validates :post_id, presence: true
  
  def new_attributes
    {
      id: self.id,
      comment_text: self.comment_text,
      profile: self.profile&.new_attributes,
      post: self.post,
      parent: self.parent&.reply_comment_attributes,
      children: self.children&.map(&:reply_comment_attributes),
      likes_count: self.likes.count,
      likers: self.likes.map { |like| like.profile},
      created_at: self.created_at
    }
  end
  
  def reply_comment_attributes
    {
      id: self.id,
      comment_text: self.comment_text,
      profile: self.profile
    }
  end
end
