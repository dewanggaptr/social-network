Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  namespace :api do
    namespace :v1 do
      resources :users
      post "/login", to: "users#login"
      resources :profiles
      resources :rooms
      resources :posts
      resources :follows
      resources :comments
      resources :likes
    end
  end
end
