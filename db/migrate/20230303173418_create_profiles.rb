class CreateProfiles < ActiveRecord::Migration[7.0]
  def change
    create_table :profiles do |t|
      t.string :profile_name
      t.string :phone_number
      t.date :dob
      t.integer :gender
      t.text :bio
      t.integer :user_id
      t.timestamps
    end
  end
end
