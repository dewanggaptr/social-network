# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

# 10.times do
#   User.create(
#     email: Faker::Internet.email,
#     password: "password"
#   )
# end

# 10.times do
#   Profile.create(
#     profile_name: Faker::Name.unique.name,
#     phone_number: Faker::Base.numerify('0###########'),
#     dob: Faker::Date.birthday(min_age: 17, max_age: 60),
#     gender: Faker::Number.between(from: 1, to: 2),
#     bio: Faker::Lorem.paragraph,
#     user_id: User.all.sample.id
#   )
# end

# 10.times do
#   Room.create(
#     title: Faker::Lorem.sentence,
#     description: Faker::Lorem.paragraph
#   )
# end

# 8.times do
#   Post.create(
#     caption: Faker::Lorem.sentence,
#     tipe: Faker::Number.between(from: 1, to: 2),
#     profile_id: Profile.all.sample.id,
#     room_id: Room.all.sample.id
#   )
# end

# 8.times do
#   Comment.create(
#     comment_text: Faker::Lorem.sentence,
#     profile_id: Profile.all.sample.id,
#     post_id: Post.all.sample.id,
#     # parent_id: Comment.all.sample.id
#   )
# end

# 10.times do
#   Follow.create(
#     follower_id: Profile.all.sample.id,
#     following_id: Profile.all.sample.id
#   )
# end

p 'Berhasil menjalankan seed'
